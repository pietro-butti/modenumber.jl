"""
    This is a `julia` package to analyze and calculate mass anomalous dimension from SU2Nf1Adj ensembles.
"""
module modenumber

    # using DelimitedFiles, PyPlot, Statistics, jkvar, Measurements, LsqFit, LaTeXStrings, ProgressMeter
    using DelimitedFiles, PyPlot, Statistics, ADerrors, Measurements, LsqFit, LaTeXStrings, ProgressMeter

    include("modenumber_types.jl")
    include("modenumber_windowing.jl")

    export ensemble
    export bootstrap, bootfit, windowing, weight_results

end 