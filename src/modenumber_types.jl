mutable struct ensemble
    tag::String
    beta::Float64
    mass::Float64
    volume::Int64

    scale::Measurement

    Nconf::Int64
    M::Vector{Float64}
    nu::Vector{uwreal}
    plaq::Vector{uwreal}

    function ensemble(tag,beta,mass,volume; path=".", name=".", input_type="HiRep", wpm=0, pruner=1)
        this = new(tag,beta,mass,volume)

        if input_type=="HiRep"
            # catnu = (name!="." ? readdlm("$path/$name") : readdlm("$path/nu_$tag.dat"))
            # catnu = (name!="." ? readdlm("$path/$name") : readdlm("$path/nu_$tag.dat"))
            catnu = readdlm("$name")

            Ω = Float64.(catnu[:,2]); Ωs = unique(Ω)
            nu = Float64.(catnu[:,5])

            # this.scale = jkreal(0.,0.)
            this.scale = 0. ± 0.000000001

            Ntot = length(Ω)
            Nomega = length(Ωs)
            Nconf = Ntot÷Nomega

            this.Nconf = Nconf

            this.M = Ωs
            # this.nu = Vector{jkreal}(undef,Nomega)
            this.nu = Vector{uwreal}(undef,Nomega)
            for ω in 1:Nomega
                range = (1:Nconf) .+ (ω-1)*Nconf
                this.nu[ω]    = uwreal(nu[range],tag)
            end

        elseif input_type=="xyerrfile"
            catnu = readdlm("$name")
            
            this.M = Float64.(catnu[:,1])
            # this.nu = [jkreal(v,e) for (v,e) in zip(catnu[:,2],catnu[:,3])]
            this.nu = [uwreal([v,e],tag) for (v,e) in zip(catnu[:,2],catnu[:,3])]

        elseif input_type=="colconf"
            catnu = readdlm("$name")

            # iiok = [i for i in 1:size(catnu,1) if (i-1)%pruner==0]

            this.M = Float64.(catnu[:,1])
            # this.nu = [jkreal(catnu[i,2:end]) for i in axes(catnu,1)]
            this.nu = [uwreal(catnu[i,2:end],tag) for i in axes(catnu,1)]
        end

        return this
    end
end
function Base.show(io::IO, a::ensemble)
    print(io,"Ensemble $(a.tag). Some specifics: [β = $(a.beta), m = $(a.mass), V = $(a.volume)]")
end
