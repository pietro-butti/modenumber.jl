"""
    bootstrap(vector,Nboot_bin,Npick)
"""
function bootstrap(vector,Nboot_bin,Npick)
    Nconf = length(vector[1].ensemble)

    yboot = Vector{Vector{jkreal}}(undef,Nboot_bin)
    for b in 1:Nboot_bin
        bootconfii = [rand(1:Nconf) for _ in 1:Npick]
        yboot[b] = [jkreal(el.ensemble[bootconfii]) for el in vector]
    end

    return yboot
end

"""
    singlefit(x,y)
"""
function singlefit(model,x,y,p0)
    fit = curve_fit(model,x,val.(y),1.0 ./ err.(y).^2,p0)
    return fit
end

"""
    bootfit(x,y,Nconf_boot,Npick)
"""
function bootfit(x,y,Nconf_boot,Npick)
    # Perform an initial fit with 3 parametersm, then 4, then alltogether
    @. model0(x,p) = p[1]*(x^2 - p[2])^p[3]
    p0 = [1.4e6,0.0005,1.05]
    lb = [-Inf,0.,0.5]
    ub = [Inf,0.1,10.0]
    fit0 = curve_fit(model0,x,val.(y),1.0 ./ err.(y).^2 ,p0,lower=lb,upper=ub)
   
    pp = coef(fit0)
    @. model1(x,p) = pp[1]*(x^2-pp[2])^pp[3] + p[1]
    p0 = [1000.]
    lb = [0.0]
    ub = [Inf]
    fit1 = curve_fit(model1,x,val.(y),1.0 ./ err.(y).^2,p0,lower=lb,upper=ub)

    @. model2(x,p) = p[1]*(x^2 - p[2])^p[3] + p[4]
    p0 = vcat(coef(fit0),coef(fit1))
    lb = [-Inf,0., 0.5,0. ]
    ub = [Inf,0.1,10.0,Inf]
    try
        fit2 = curve_fit(model2,x,val.(y),1.0 ./ err.(y).^2 ,p0,lower=lb,upper=ub)
        p0 = coef(fit2)
    catch
        0
    end
    # Create the bootstrap sample
    yboot = bootstrap(y,Nconf_boot,Npick)

    # Perform fit in each boot bin
    fits = []
    fails = 0
    @showprogress for (b,yb) in enumerate(yboot)
        try
            fit = curve_fit(model2,x,val.(yb),1.0 ./ err.(yb).^2 ,p0,lower=lb,upper=ub)
            push!(fits,fit)
        catch
            fails += 1
            continue
        end
    end

    fails==Nconf_boot ? println("No one converged for [$(x[1]):$(x[end])]") : 0

    # 0 values filtering
    iis = [i for (i,f) in enumerate(fits) if coef(f)[2]!=0.0 && coef(f)[4]!=0.0]
    F = fits[iis]
    fails += length(iis)

    return F,fails
end

"""
    windowing(X,Y,Ωmin,ΔΩ,p0)
    windowing(X,Y,Ωmin,ΔΩ,Nconf_boot,Npick)
"""
function windowing(model::Function,X::Vector,Y::Vector,Ωmin,ΔΩ,p0::Vector)
    ϕ = []
    Ωrange = []
    for xmin in Ωmin
        for δ in ΔΩ
            xmax = xmin + δ
            ii = [i for (i,el) in enumerate(X) if el>=xmin && el<=xmax ]
            x = X[ii]
            y = Y[ii]

            push!(Ωrange,[xmin,xmax])
            println("Window:     [$xmin,$(round(xmin+δ,digits=3))] ---> n. points: $(length(ii))")
            try
                fit = singlefit(model,x,y,p0)
                println("    χ² = ",sum(fit.resid.^2)/dof(fit))
                println("    γ* = ",2.0/(coef(fit)[3])-1.0)
                push!(ϕ,fit)
            catch
                println("    ---> fit not converged!")
                push!(ϕ,0)
            end
        end
    end
    return ϕ,Ωrange
end
function windowing(X,Y,Ωmin,ΔΩ,Nconf_boot,Npick)
    ϕ = []
    fails = []
    Ωrange = []
    for xmin in Ωmin
        for δ in ΔΩ
            xmax = xmin + δ
            ii = [i for (i,el) in enumerate(X) if el>=xmin && el<=xmax ]
            x = X[ii]
            y = Y[ii]
    
            length(x)<5 ? continue : 0
            
            F,nope = bootfit(x,y,Nconf_boot,Npick)
            
            push!(ϕ,F)
            push!(fails,nope)
            push!(Ωrange,[xmin,xmax])
    
            println("Window:     [$xmin,$(round(xmin+δ,digits=3))] ---> n. points: $(length(ii))")
            println("Fails rate: $(nope/Nconf_boot)%")
        end
    end

    return ϕ, fails, Ωrange
end


function weight_results(f::Vector,fails::Vector,ranges::Vector,Nconf_boot::Int64)
    lengths = [length([length(coef(ff)) for ff in fit]) for (i,fit) in enumerate(f)]
    iiok = setdiff(collect(1:length(f)),[i for (i,l) in enumerate(lengths) if l==0||l==1])
    
    γ    = [jkreal(mean([2.0/(coef(ff)[3])-1.0 for ff in fit]),std([2.0/(coef(ff)[3])-1.0 for ff in fit])) for fit in f[iiok]]
    chis = [mean([sum(ff.resid.^2) for ff in fit]) for fit in f[iiok]]
    dofs = [mean([dof(ff) for ff in fits]) for fits in f[iiok]]
    α    =  fails[iiok]./Nconf_boot
    σ    = err.(γ) ./ val.(γ)

    w1 = chis
    w2 = dofs
    w12 = (sqrt.(w1./w2).-1.0).^2
    w3 = α
    w4 = σ; w4 = w4./max(w4...)

    AICw1 = w12+w3+w4; 
    W1 = exp.(.-AICw1); W1 ./= sum(W1)

    γfin = sum(W1.*γ)
    errγ = sqrt(val(sum(W1.*γ.^2) - (sum(W1.*γ))^2))

    return γfin, errγ, W1, γ
end