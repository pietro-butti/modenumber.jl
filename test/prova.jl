using Revise, modenumber

ensembles = Dict(
    "DB1M8"  => ("HiRep"  ,pruner=1 ),
    "DB1M10" => ("colconf",pruner=10),
    "DB2M7b" => ("HiRep"  ,pruner=1 ),
    "DB3M8"  => ("HiRep"  ,pruner=1 ),
    "DB4M11" => ("HiRep"  ,pruner=1 ),
    "DB4M13" => ("colconf",pruner=10),
    "DB6M9"  => ("colconf",pruner=10),
    "DB7M7"  => ("colconf",pruner=10),
    "DB7M10" => ("colconf",pruner=10)
)

DATA = Dict()
for (id,(format)) in sort(ensembles)
    try
        println(id)
        DATA[id] = ensemble(id,0.,0.,0.; 
            name="/Users/pietro/Data/modenumber/$id/dataconfigs.txt",
            input_type=format
        )
    catch
        println("Problems with $id")
        continue
    end
end