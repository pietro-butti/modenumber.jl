using DelimitedFiles, PyPlot, Statistics, Measurements, LsqFit, LaTeXStrings, ProgressMeter, jkvar

include("../src/modenumber_types.jl")
include("../src/modenumber_windowing.jl")

path = "/home/pietro/Desktop"
tag = "TAG"

e = ensemble("TAG", 2.05,1.524, 663552, path=path, name="data.txt", input_type="xyerrfile")
X = e.Ω
Y = e.ν


Ωmin = 0.03:0.01:0.15
ΔΩ   = 0.02:0.02:0.1


@. model(x,p) = p[1]*(x^2 - p[2])^p[3]
p0 = [1.4e6,0.0005,1.05]

ϕ,ranges = windowing(model,X,Y,Ωmin,ΔΩ,p0)

##

# c3 = [coef(f)[3]±stderror(f)[3] for f in ϕ]
c3 = [jkreal(coef(f)[3],stderror(f)[3]) for f in ϕ]
g = 2.0 ./(c3) .-1.0


iiok = [i for (i,f) in enumerate(ϕ) if sum(f.resid.^2)/dof(f)<0.45]


xx = [r[1] for r in ranges]
ss = 10 .*exp.([r[2] for r in ranges])
chis = [sum(f.resid.^2)/dof(f) for f in ϕ]


w = exp.(-chis)
w /= sum(w)


scatter(xx,val.(g),s=ss,c=chis)
colorbar()


xlabel("Ω min")
ylabel("γ*")
behold("prova.pdf")
##

